"""
Checks that DCS installation directory for any extra files that might be there.
Check is based on data from autoupdate.dat file.
"""

import sqlite3
import os
import sys
import shutil


def is_ignored(file_name):
    for ignored_file in ignore_list:
        if file_name.startswith(ignored_file):
            return True
    return False


def normalize_filename(file_name):
    file_name_norm = file_name.replace('\\', '/')
    file_name_norm = file_name_norm.strip()
    file_name_norm = unicode(file_name_norm)
    return file_name_norm


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: extra_file_checker.exe dcs_install_directory"
        print "Example: extra_file_checker.py C:\dcs"

    dcs_installation = sys.argv[1]
    script_location = os.path.dirname(os.path.abspath(__file__))
    database_file_name = "autoupdate.dat"
    ignore_list_file_name = "ignore.txt"

    # path in quotes plus backslash is deadly combination
    if dcs_installation[len(dcs_installation) - 1] == '"':
        dcs_installation = dcs_installation[:-1]

    real_database_file = os.path.join(dcs_installation, database_file_name)
    if not os.path.exists(real_database_file):
        print "File %s not found in %s\n" \
              "Please make sure that path above is really pointing to DCS installation directory" \
              % (database_file_name, dcs_installation)
        sys.exit(1)

    # perform operation over backup file, not the real one
    database_temp_dir = os.path.join(script_location, "temp")
    if not os.path.exists(database_temp_dir):
        os.mkdir(database_temp_dir)
    database_file = os.path.join(database_temp_dir, database_file_name)
    if os.path.exists(database_file):
        os.remove(database_file)
    shutil.copy2(real_database_file, database_file)

    print "Loading file list from %s ..." % real_database_file
    database = sqlite3.connect(database_file)
    cursor = database.cursor()
    internal_list = []

    # get info about files from database
    cursor.execute('SELECT filename FROM installed')
    filenames = cursor.fetchall()
    for filename in filenames:
        internal_list.append(filename[0])
    database.close()


    # based on data from db, get also list of directories
    directories = set()
    for file_name in internal_list:
        file_name_to_dissect = file_name
        '''
        go to the root and cut away file/dir names along the way. This way, we'll get also directories that contains
        only directories, not files (and therefore are not present in autoupdate.dat
        '''
        while '/' in file_name_to_dissect:
            file_name_to_dissect = file_name_to_dissect[:file_name_to_dissect.rindex('/')]
            directories.add(file_name_to_dissect)

    # add directories to internal file names
    for directory in directories:
        internal_list.append(directory)


    # load also ignore list
    ignore_list = []
    ignore_list_file_path = os.path.join(script_location, ignore_list_file_name)
    print "Loading ignore list from %s ..." % ignore_list_file_path
    ignore_list_file = open(ignore_list_file_path, 'r')
    if not os.path.exists(ignore_list_file_path):
        print "WARNING: Ignore list doesn't exist"
    else:
        for file_to_ignore in ignore_list_file:
            file_to_ignore_norm = file_to_ignore.replace('\\', '/')
            file_to_ignore_norm = file_to_ignore_norm.strip()
            file_to_ignore_norm = unicode(file_to_ignore_norm)
            ignore_list.append(normalize_filename(file_to_ignore))

    print "Scanning %s for any extra files or directories ..." % dcs_installation
    dcs_installation_list = []
    for root, dirs, files in os.walk(unicode(dcs_installation)):
        # files
        for file in files:
            full_file_path = os.path.join(root, file)
            relative_file_path = full_file_path.replace(dcs_installation, "")
            if relative_file_path[0] == '\\':
                relative_file_path = relative_file_path[1:]
            relative_file_path = relative_file_path.replace('\\', '/')
            dcs_installation_list.append(relative_file_path)

        # dirs
        for dir in dirs:
            full_file_path = os.path.join(root, dir)
            relative_file_path = full_file_path.replace(dcs_installation, "")
            if relative_file_path[0] == '\\':
                relative_file_path = relative_file_path[1:]
            relative_file_path = relative_file_path.replace('\\', '/')
            dcs_installation_list.append(relative_file_path)

    extra_files_and_dirs = []

    print "Performing compare ..."
    extra_files_and_dirs = []
    for installation_file in dcs_installation_list:
        if installation_file not in internal_list:
            if is_ignored(installation_file):
                continue
            extra_file = os.path.join(dcs_installation, installation_file)
            extra_file = extra_file.replace('/', '\\')
            print "Extra: %s" % extra_file
            extra_files_and_dirs.append(extra_file)

    results_file_name = os.path.join(script_location, "results.txt")
    results_file = open(results_file_name, 'w')

    results_file.write("Extra files or directories:\n")
    for extra_file in extra_files_and_dirs:
        results_file.write("%s\n" % extra_file)

    print "\nCheck finished, results are stored in %s" % results_file_name

    # delete temp database
    os.remove(database_file)
    os.rmdir(database_temp_dir)      